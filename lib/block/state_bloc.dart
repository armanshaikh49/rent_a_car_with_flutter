import 'state_provider.dart';
import 'dart:async';

class StateBloc {
  StreamController animationController = StreamController();
  final StateProvider provider = StateProvider();

  Stream get animationStatus => animationController.stream;

  void toggleAnimation(){
    provider.toggleAnimationValue();
    animationController.sink.add(provider.isAnimating);
  } // toggleanimation
  void dispose() {
    animationController.close();
  } //dispose
} //stateblock

final stateBloc = StateBloc();